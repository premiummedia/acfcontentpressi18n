<?php

namespace acfcontentpressi18n\actions;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

add_action('acfcp/registered_post_type', 'acfcontentpressi18n\\actions\\registered_post_type', 1, 2);


function registered_post_type($cptObject, $cptSettings)
{
    global $wpdb;

    $names = [];
    $slug = $cptObject->name;

    $languageKeys = array_keys(Config::languages());

    if (array_key_exists('translations', $cptSettings)) {
        $names = $cptSettings['translations'];
    }

    if (empty($names)) {
        return false;
    }

    $names = array_diff($names, array_flip($languageKeys));
    
    $tableName = $wpdb->prefix.Config::$cptSlugTranslationTable;

    foreach ($names as $language => $name) {
        $id = $wpdb->get_var('SELECT id FROM '.$tableName.' WHERE cpt = "'.$slug.'" AND lang = "'.$language.'"');
        if (!$id) {
            $wpdb->insert(
                $tableName,
                array(
                    'cpt' => $slug,
                    'lang' => $language,
                    'slug' => $name
                )
            );
        } else {
            $wpdb->update(
                $tableName,
                array(
                    'slug' => $name
                ),
                array(
                    'id' => $id
                )
            );
        }
    }

    return $slug;
}
