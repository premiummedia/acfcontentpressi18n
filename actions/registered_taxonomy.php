<?php

namespace acfcontentpressi18n\actions;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

add_action('acfcp/registered_taxonomy', 'acfcontentpressi18n\\actions\\registered_taxonomy', 1, 2);

function registered_taxonomy($taxonomy, $names)
{
    global $wpdb;

    $languageKeys = array_keys(Config::languages());

    if (!is_array($names)) {
        return false;
    }

    $tableName = $wpdb->prefix.Config::$taxonomySlugTranslationTable;

    foreach ($names as $language => $name) {
        $id = $wpdb->get_var('SELECT id FROM '.$tableName.' WHERE taxonomy = "'.$taxonomy.'" AND lang = "'.$language.'"');
        if (!$id) {
            $wpdb->insert(
                $tableName,
                array(
                    'taxonomy' => $taxonomy,
                    'lang' => $language,
                    'slug' => $name
                )
            );
        }
    }

    return $taxonomy;
}
