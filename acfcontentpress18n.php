<?php
/**
 * @package ACF ContentPress I18N
 */
/*
Plugin Name: ACF ContentPress I18N
Plugin URI: https://premiummedia.ch/acf-contentpress-i18n
Description: Multilanguage Support for ACF CP
Version: 1.0.0-alpha.6
Author: premiummedia.ch
License: GPLv2 or later
Text Domain: acf-contentpress-i18n
*/

namespace acfcontentpressi18n;

defined('ABSPATH') or die();
require "autoload.php";
require "settings.php";


use acfcontentpressi18n\Config;
use acfcontentpressi18n\routing\Router;
use acfcontentpressi18n\fields\SlugFields;

if (Config::i18nActive()) {

    if( is_admin() ){
        require "admin/filters/permalinks.php";
    }

    // add additional fields for translated fields
    require "filters/addi18nfield.php";
    // add admin js helpers for slugs and styling
    require "js/scripts.php";
    require "filters/layoutdata.php";

    require "filters/navigation.php";
    require "filters/attachments.php";

    require "filters/title.php";
    require "filters/settingsactions.php";

    require "filters/get_the_title.php";

    function acfcpi18nRegisterSlugFields()
    {
        $slugFieldGroup = new SlugFields;
        $slugFieldGroup->updateKeys();
        \acf_add_local_field_group($slugFieldGroup->getSettings());
    }

    add_action('acf/init', 'acfcontentpressi18n\\acfcpi18nRegisterSlugFields');

    require "filters/slugs.php";
    new Router();

    require "actions/registered_post_type.php";
    require "actions/registered_taxonomy.php";

    function addTaxTermFieldGroup($fieldGroupStrings)
    {
        array_push($fieldGroupStrings, 'acfcontentpressi18n\\fields\\TaxTermLabelFields');
        return $fieldGroupStrings;
    }

    add_filter('acfcp/registerContentStrings', 'acfcontentpressi18n\\addTaxTermFieldGroup');
}

function install()
{
    require 'db/setup.php';
}
register_activation_hook(__FILE__, 'acfcontentpressi18n\\install');
