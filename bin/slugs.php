<?php
use acfcontentpress\Config;
use acfcontentpress\register\RegisterHelper;

global $wpdb;

$tableSlugs = 'wp_slugs';

$wpdb->query('TRUNCATE TABLE wp_slugs');

$cptFolder = Config::getPostTypeFolder();

$cptConfigs = RegisterHelper::loadConfigArrays($cptFolder);

$cpts = array_keys($cptConfigs);

array_push($cpts, 'page', 'post');

$allPostsQuery = new WP_Query([
    'post_type' => $cpts,
    'posts_per_page' => -1,
]);

foreach ($allPostsQuery->posts as $post) {
    acfcontentpressi18n\filters\saveSlugs($post->ID);
}
