<?php
namespace acfcontentpressi18n\fields;

defined('ABSPATH') or die();

use acfcontentpress\core\FieldGroup;
use acfcontentpress\contrib\fields\TextField;
use acfcontentpressi18n\Config;

class TaxTermLabelFields extends FieldGroup
{
    public function __construct()
    {
        parent::__construct('taxtermlabels', 'Taxonomy Term Labels', array(
            'position' => 'side',
            'location' => array(
                array(
                    array(
                        'param' => 'taxonomy',
                        'operator' => '==',
                        'value' => 'all'
                    )
                )
            )
        ));

        foreach (Config::languages() as $key => $langLabel) {
            $this->addField(
                new TextField(
                    'taxtermlabel_'.$key,
                    $langLabel.' Label',
                    array(
                        'instructions' => 'Translated version of the name'
                    )
                )
            );
        }
    }
}
