<?php
namespace acfcontentpressi18n\fields;

defined('ABSPATH') or die();

use acfcontentpress\core\FieldGroup;
use acfcontentpress\contrib\fields\TextField;
use acfcontentpressi18n\Config;
use acfcontentpress\Config as ACFCPConfig;
use acfcontentpress\register\RegisterHelper;

class SlugFields extends FieldGroup
{
    public function __construct()
    {
        $slugLocation = array();

        $slugLocation = array_map(
            function ($loc) {
                return array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => $loc
                    ),
                );
            },
            array_keys(
                RegisterHelper::loadConfigArrays(
                    ACFCPConfig::getPostTypeFolder()
                )
            )
        );

        // ... and taxonomies
        $slugLocation = array_merge(
            array(
                array(
                    array(
                        'param' => 'taxonomy',
                        'operator' => '==',
                        'value' => 'all'
                    )
                )
            ),
            $slugLocation
        );
        $slugLocation = array_merge(
            array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page'
                    )
                )
            ),
            $slugLocation
        );
        $slugLocation = array_merge(
            array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post'
                    )
                )
            ),
            $slugLocation
        );

        parent::__construct(
            'slugs',
            'I18N Slugs',
            array(
                'position' => 'side',
                'style' => 'seamless',
                'location' => $slugLocation
            )
        );

        foreach (Config::languages() as $key => $label) {
            $this->addField(
                new TextField(
                    'slug_'.$key,
                    'Slug '.$label,
                    array(
                        'required' => 1,
                        'instructions' => 'Translated, url-friendly (only a-z,1-9,-,_) version of the title.'
                    )
                )
            );
        }
    }
}
