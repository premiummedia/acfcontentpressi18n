<?php
namespace acfcontentpressi18n;

defined('ABSPATH') or die();

class Config
{
    public static function templateBase()
    {
        return get_template_directory();
    }

    public static $slugTranslationTable = 'slugs';
    public static $cptSlugTranslationTable = 'cptslugs';
    public static $taxonomySlugTranslationTable = 'taxslugs';

    public static function i18nActive()
    {
        return !!get_option('acfcp_options_i18n_activate_i18n');
    }

    public static function passRoutes(){
        $routes = [];
        for ( $i = 0; $i < 99; $i++ ){
            $route = get_option(
                'acfcp_options_i18n_pass_'.$i.'_route'
            );
            $method = get_option(
                'acfcp_options_i18n_pass_'.$i.'_method'
            );
            if( !$route && !$method ){
                break;
            }
            $routes[] = [
                'method' => $method,
                'route' => $route
            ];
        }
        return $routes;
    }

    public static function languages()
    {
        $languages = array();
        $i = 0;

        for ($i = 0; $i < 12; $i++) {
            $shortCode = get_option(
                'acfcp_options_i18n_languages_'.$i.'_language_short_code'
            );
            $label = get_option(
                'acfcp_options_i18n_languages_'.$i.'_language_label'
            );

            if ($shortCode && $label) {
                $languages[$shortCode] = $label;
            } else {
                break;
            }
        }

        return $languages;
    }

    public static function mainLanguage()
    {
        $mainLanguage =  get_option('acfcp_options_i18n_main_language');

        if ($mainLanguage) {
            return $mainLanguage;
        } else {
            if (current(Config::languages())) {
                return current(Config::languages());
            }
        }
    }

    public static function restrictTo(){

        $restrictTo = get_option('acfcp_options_i18n_use_main_language');

        if( $restrictTo ){
            return self::mainLanguage();
        }
        return false;

    }

    public static function mainLanguageFallback(){
        return !!get_option('acfcp_options_i18n_main_language_fallback');
    }

    public static function lang()
    {
        return apply_filters('acfcp/lang', false);
    }
}
