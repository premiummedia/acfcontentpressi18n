
jQuery(document).ready(function(){
    //var $tabs = jQuery('body :not(.clones) .acf-field-tab');
    var $tabs = jQuery('.inside.acf-fields > .acf-field-tab');
    check_tabs();

    jQuery('.inside').on('click', '.acf-tab-button', function(){
        check_tabs();
    });

    function check_tabs(){

        jQuery.each($tabs, function(){

            var $this = jQuery(this),
                klass = $this.attr('class');
            if( klass.indexOf('|') > 0 ){
                var name = $this.data('key').replace('tab|', '|');
                if( jQuery(document.getElementById('acf-'+name)).val() == '' ){
                    //console.log('a.acf-tab-button[data-key="'+name+'"]');
                    var $button = jQuery('a.acf-tab-button[data-key="'+$this.data('key')+'"]');
                    if( !$button.find('span').length ){
                        $button.append('<span style="color:red; font-weight: bold;">&nbsp;&#8226;</span>');
                    }
                }
            }
        });

    }
});
