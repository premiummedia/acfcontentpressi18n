function slugify(text)
{
    return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace('ä', 'ae')
    .replace('ö', 'oe')
    .replace('ü', 'ue')
    .replace('é', 'e')
    .replace('à', 'a')
    .replace('è', 'e')
    .replace('ê', 'e')
    .replace('ô', 'o')
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}
jQuery(document).ready(function(){

    var $postTitleInput = jQuery('input[name=post_title]'),
        $acfSlugFields = jQuery('input[type=text][name^="acf\[slugs.slug_"]'),
        $alternativeTitleInput = jQuery('.acf-field.replacesTitle');


    if( !$postTitleInput.length ){

        if( $alternativeTitleInput.length ){
            $alternativeTitleInput.on('focusin', 'input[type=text]', function(){
                jQuery(this).data('value', jQuery(this).val());
            });

            $alternativeTitleInput.on('change', function(){
                var name = jQuery(this).data('name');
                var lang = '';
                nameSplit = name.split('|');
                if( nameSplit.length > 1 ){
                    lang = nameSplit[1];
                }

                var oldValue = jQuery(this).find('input[type=text]').data('value');
                console.log(oldValue) || '',
                    newValue = slugify(jQuery(this).find('input[type=text]').val());

                if( lang ){
                    var $field = jQuery('input[type=text][name^="acf\[slugs.slug_'+lang+'\]"]');
                    if( $field.val() == slugify(oldValue) || $field.val() == "" ){
                        $field.val(newValue);
                    }
                }else{
                    jQuery.each($acfSlugFields, function(i, field){
                        var $field = jQuery(field);
                        if( $field.val() == slugify(oldValue) || $field.val() == "" ){
                            $field.val(newValue);
                        }
                    });
                }


            });
        }

    }else{
        $postTitleInput.on('focusin', function(){
            jQuery(this).data('value', jQuery(this).val());
        });

        $postTitleInput.on('change', function(){
            var oldValue = jQuery(this).data('value') || '',
            newValue = slugify(jQuery(this).val());
            jQuery.each($acfSlugFields, function(i, field){
                var $field = jQuery(field);
                if( $field.val() == slugify(oldValue) || $field.val() == "" ){
                    $field.val(newValue);
                }
            });
        });
    }

    // make sure only allowed slug chars are in the slug input field
    $acfSlugFields.on('change', function(){
        jQuery(this).val( slugify(jQuery(this).val()));
    })
});
