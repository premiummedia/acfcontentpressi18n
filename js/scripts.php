<?php
namespace acfcontentpressi18n\scripts;

use acfcontentpressi18n\Config;

function acfcpi18n_admin_enqueue($hook)
{
    wp_enqueue_script('acfcpi18ntabheight', plugin_dir_url(__FILE__)."/../../js/acfcpi18ntabheight.js");
    wp_enqueue_script('acfcpi18nmissingcontent', plugin_dir_url(__FILE__)."/../../js/acfcpi18nmissingcontent.js");
    wp_enqueue_script('acfcpi18nslugs', plugin_dir_url(__FILE__)."/../../js/acfcpi18nslugs.js");
}

add_action('admin_enqueue_scripts', 'acfcontentpressi18n\\scripts\\acfcpi18n_admin_enqueue');
