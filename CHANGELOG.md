# Change Log
All notable changes to this project will be documented in this file.

## [1.0.0-alpha.6] - 2018-004-10

### Added

- WordPress Title Support: WP functions the_title and get_the_title will return the value of a translated acf field on the current post called 'title'

### Removed

- https setting

### Fixed

- Only create field translations when translate => true
- Only load admin permalink when in admin screen

### Changed

- createField filter compatible with acfcp 1.2.4
- Actions in seperate folder

## [1.0.0-alpha.3] - 2018-04-02

### Changed

- Allow for standard permalinks if acfcp doesnt know how to deal with it

## [1.0.0-alpha.3] - 2018-04-02

### Added
- Settings and functionality to ignore routes and restrict to main language

## [1.0.0-alpha.1] - 2018-03-16

### Changed
- The way additional fields for each language are created

### Added
- Settings option to allow a fallback to the main language for field values

## [1.0.0-alpha] - 2018-03-16

### Changed

- $view object: is available everywhere. use $view->get_header() and $view->get_footer() instead of standard wp functions.

## [0.3.5] - 2017-12-22

### Fixed
- Allow for external links in navigation

## [0.3.2] - 2017-09-05

### Fixed
- Fix menu to use active language links

## [0.3.1] - 2017-09-04

### Changed
- Fix menu walker filter to run later and actually overwrite stuff

## [0.3] - 2017-08-22

### Changed
- Introduce a pseudo-field for translations

### Fixed
- Return meaningful error message when a template isn't found
- Don't depend on cpt definition for slug field registration

## [0.2.1] - 2017-06-06

### Added
- Added defined('ABSPATH') condition to prevent direct execution

### Fixed
- Slugs automatically generated from title are actually slugs

## [0.2] - 2017-05-30

### Added
- Replace title multilang support

### Changed
- Split JS code in smaller files

### Fixed
- Permalink generation

## [0.1.2] - 2017-05-23

### Fixed
- Check if Contents property actually exists before using it

## [0.1.1] - 2017-05-23

### Changed
- Remove unnecessary autoload locations

## [0.1.0] - 2017-05-23

### Added
- Use HTTP accept language to determine active frontend language
- Filters for attachments
- Taxonomy translations
- Navigation
- Routing

### Fixed
- Config language usage
