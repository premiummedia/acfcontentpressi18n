<?php

namespace acfcontentpressi18n\filters;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

use acfcontentpress\register\FieldFactory;

function fieldAddFieldFields($inputFieldDefs){

    $fieldDefs = [];
    foreach( $inputFieldDefs as $cleanFieldDef ){
        if( array_key_exists( 'translate', $cleanFieldDef['settings'] ) && $cleanFieldDef['settings']['translate'] ){

            $index = 0;
            $ogKey = $cleanFieldDef['key'];
            foreach( Config::languages() as $langKey => $langLabel ){

                $endPoint = '0';
                if( $index == 0 ){
                    $endPoint = '1';
                }
                
                $fieldDefs = array_merge( $fieldDefs, [
                    [
                        \ACFCP::TABFIELD,
                        $ogKey."tab|".$langKey,
                        $langLabel,
                        [
                            'placement' => 'left',
                            'endpoint' => ( $index == 0 ? true : false ) 
                        ]
                    ],
                    [
                        $cleanFieldDef['klass'],
                        $ogKey."|".$langKey,
                        $cleanFieldDef['label'],
                        $cleanFieldDef['settings'],
                        'layouts' => $cleanFieldDef['layouts'],
                        'fields' => $cleanFieldDef['subfields'],
                    ]
                ]);
                $index++;

            } 
            $fieldDefs = array_merge($fieldDefs, [
                [
                    \ACFCP::ENDPOINTTABFIELD,
                    'a',
                    'b',
                    []
                ]
            ]);
            $fieldDefs = array_map(function($fd){
                return FieldFactory::getCleanDefinition($fd);
            }, $fieldDefs);
            

        }else{
            $fieldDefs[] = $cleanFieldDef;
        }
        
    }
    return $fieldDefs;

}

add_filter('acfcp/fieldAddFieldFields', 'acfcontentpressi18n\\filters\\fieldAddFieldFields');

function i18nValue($value, $data, $key){

    $i18nValue = null;
    $mainLangValue = null;
    $langKey = apply_filters('acfcp/lang', '');

    $isThereAFieldTranslation = array_filter($data, function($rowKey) use ($key){
        return (strpos($rowKey, $key."|", 0) === false ? false: true);
    }, ARRAY_FILTER_USE_KEY);

    if( !$isThereAFieldTranslation ){
        return $value;
    }

    if( array_key_exists($key."|".$langKey, $data) ){
        $i18nValue = $data[$key."|".$langKey];
        if( !empty($i18nValue) ){
            return $i18nValue;
        }
    }

    if( Config::mainLanguageFallback() ){
        if( array_key_exists($key."|".Config::mainLanguage(), $data) ){
            $mainLangValue = $data[$key."|".Config::mainLanguage()];
            if( !empty($mainLangValue) ){
                return $mainLangValue;
            }
        }
    }

    return null;


}
add_filter('acfcpfe/contentvalue', 'acfcontentpressi18n\\filters\\i18nValue', 1, 3);
