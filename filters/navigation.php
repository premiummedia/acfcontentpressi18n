<?php
namespace acfcontentpressi18n\filters;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

add_action('wp_update_nav_menu_item', 'acfcontentpressi18n\\filters\\update_nav_menu_item_slugs', 10, 3);

add_filter('wp_setup_nav_menu_item', 'acfcontentpressi18n\\filters\\setup_nav_menu_item_slugs');

add_filter('wp_edit_nav_menu_walker', 'acfcontentpressi18n\\filters\\edit_nav_menu_walker', 99, 2);


function edit_nav_menu_walker($walker, $menu_id)
{
    if (defined('DOING_AJAX') && DOING_AJAX) {
        return $walker;
    }
    return 'acfcontentpressi18n\\navigation\\I18nWalker';
}


function update_nav_menu_item_slugs($menu_id, $menuItemId, $args)
{
    foreach (Config::languages() as $lang => $language) {
        if (array_key_exists('menu-item-label-'.$lang, $_REQUEST) && is_array($_REQUEST['menu-item-label-'.$lang]) && isset($_REQUEST['menu-item-label-'.$lang][$menuItemId])) {
            $value = $_REQUEST['menu-item-label-'.$lang][$menuItemId];
            update_post_meta($menuItemId, '_menu_item_label_'.$lang, $value);
        }
    }
}

function setup_nav_menu_item_slugs($menuItem)
{
    foreach (Config::languages() as $lang => $language) {
        $menuItem->{'menu_item_label_'.$lang} = get_post_meta($menuItem->ID, '_menu_item_label_'.$lang, true);
    }

    return $menuItem;
}
