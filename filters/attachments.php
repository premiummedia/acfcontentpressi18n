<?php

namespace acfcontentpressi18n\filters;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

add_filter('attachment_fields_to_edit', 'acfcontentpressi18n\\filters\\mediaSlugFieldsEdit', null, 2);

add_filter('attachment_fields_to_save', 'acfcontentpressi18n\\filters\\mediaSlugFieldsSave', 10, 2);

function getAttachmentFields()
{
    return array(
           'title' => 'Title',
           'alt' => 'Alt Text',
           'caption' => 'Caption'
       );
}

function mediaSlugFieldsEdit($formFields, $post)
{
    $fields = getAttachmentFields();
	
    foreach (Config::languages() as $langKey => $language) {
        foreach ($fields as $fieldKey => $label) {
            $formFields[$fieldKey.'_'.$langKey] = array(
               'label' => $label.' '.$language,
               'input' => 'text',
               'value' => get_post_meta($post->ID, $fieldKey.'_'.$langKey, true)
           );
        }
    }

    return $formFields;
}

function mediaSlugFieldsSave($post, $attachment)
{
	$fields = getAttachmentFields();
	
    foreach (Config::languages() as $langKey => $language) {
        foreach ($fields as $fieldKey => $label) {
            $fieldName = $fieldKey.'_'.$langKey;
            if (isset($attachment[$fieldName])) {
                update_post_meta($post['ID'], $fieldName, $attachment[$fieldName]);
            }
        }
    }

    return $post;
}
