<?php
namespace acfcontentpressi18n\filters;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;
use acfcontentpressi18n\db\DBTranslations;

add_filter('acf/save_post', 'acfcontentpressi18n\\filters\\saveSlugs', 20);
add_filter('delete_post', 'acfcontentpressi18n\\filters\\deleteSlugs', 10);


function saveSlugs($postId)
{
    global $wpdb;

    if (wp_is_post_revision($postId)) {
        return;
    }

    $tableName = $wpdb->prefix.Config::$slugTranslationTable;

    $postType = get_post_type($postId);
    $parentId = wp_get_post_parent_id($postId);
    $parentArray = ($parentId) ? array('parent' => $parentId) : array();

    foreach (array_keys(Config::languages()) as $key) {
        $slug = get_field('slugs.slug_'.$key, $postId);

        if (empty($slug)) {
            $slug = get_post_field('post_name', $postId);
        }
        $slug = str_replace(' ', '-', $slug);
        $slug = trim($slug);
        $slug = strtolower($slug);
        $slug = str_replace('ä', 'ae', $slug);
        $slug = str_replace('ö', 'oe', $slug);
        $slug = str_replace('ü', 'ue', $slug);
        $slug = str_replace('é', 'e', $slug);
        $slug = str_replace('è', 'e', $slug);
        $slug = str_replace('à', 'a', $slug);
        $slug = str_replace('ê', 'e', $slug);
        $slug = str_replace('ô', 'o', $slug);
        if ($slug) {
            $row = $wpdb->get_row('SELECT id, slug FROM '.$tableName.' WHERE lang = "'.$key.'" AND type = "'.$postType.'" AND slug = "'.$slug.'" AND NOT post_id = '.$postId, OBJECT);


            if ($row) {

                // Slug already exists, add
                $existingSlug = $row->slug;
                $matches = array();
                $next = 2;
                $skipFirstSlug = false;
                if (preg_match('/(\d+)$/', $existingSlug, $matches)) {
                    $number = (int)$matches[1];
                    $next = $number++;
                } else {
                    $slug = $slug."-".$next;
                    $skipFirstSlug = true;
                }

                do {
                    if ($skipFirstSlug) {
                        $skipFirstSlug = false;
                        $number = $next;
                    } else {
                        $slug = substr($slug, 0, -strlen((string)($next-1))).$next;
                    }
                    $id = $wpdb->get_var('SELECT id FROM '.$tableName.' WHERE lang = "'.$key.'" AND type = "'.$postType.'" AND slug = "'.$slug.'"');
                    $next++;
                } while ($id);

                update_field('slugs.slug_'.$key, $slug, $postId);
            }

            $id = $wpdb->get_var('SELECT id FROM '.$tableName.' WHERE post_id = '.$postId.' AND lang = "'.$key.'" AND type = "'.$postType.'"');

            if (!$id) {
                $wpdb->insert($tableName, array_merge(array(
                    'post_id' => $postId,
                    'type' => $postType,
                    'lang' => $key,
                    'slug' => $slug
                ), $parentArray));
            } else {
                $wpdb->update(
                    $tableName,
                    array_merge(
                        array('slug' => $slug),
                        $parentArray
                    ),
                    array('id' => $id)
                );
            }
        }
    }
}

 function deleteSlugs($postId)
 {
     global $wpdb;

     $tableName = $wpdb->prefix.Config::$slugTranslationTable;

     $wpdb->delete($tableName, array('post_id' => $postId), array('%d'));
 }
