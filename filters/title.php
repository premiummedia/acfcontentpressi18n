<?php

namespace acfcontentpressi18n\filters;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

add_filter('acfcp/replaceTitleFieldKey', 'acfcontentpressi18n\\filters\\replaceTitle');

add_filter('acfcp/replaceTitleFieldKeyFrontend', 'acfcontentpressi18n\\filters\\replaceTitleFrontend');

function replaceTitle($searchFor)
{
    return $searchFor."|".Config::mainLanguage();
}

function replaceTitleFrontend($searchFor)
{
    $activeLanguage = apply_filters('acfcp/lang', '');
    return $searchFor."|".$activeLanguage;
}
