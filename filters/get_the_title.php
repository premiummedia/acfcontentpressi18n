<?php

namespace acfcontentpressi18n\filters;

function get_the_title($title, $id){
    $lang = apply_filters('acfcp/lang', '');
    $fields = get_fields($id);
    foreach( $fields as $key => $value ){
        if( strpos( $key, 'title|'.$lang ) !== false ){
            return $value;
        }
    }
}


add_filter('the_title', 'acfcontentpressi18n\\filters\\get_the_title', 1, 2);
