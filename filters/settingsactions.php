<?php
function save_acfcpi18n_options_page($postId)
{
    $screen = get_current_screen();
    if (strpos($screen->id, "acf-options-acf-cp-i18n") == true) {
        if (is_array($_POST) && array_key_exists('acf', $_POST) && $postAcf = $_POST['acf']) {
            $regenSlugs = $postAcf['field_regenerate_slugs'];
            if ($regenSlugs) {
                require_once __DIR__."/../bin/slugs.php";
            }
            $_POST['acf']['field_regenerate_slugs'] = 0;
        }
    }
}
add_action('acf/save_post', 'save_acfcpi18n_options_page', 5);
