<?php
namespace acfcontentpressi18n\filters;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

function filterLanguage($fields)
{
    $lang = apply_filters('acfcp/lang', '');

    $return = array();
    if (!is_array($fields)) {
        return $fields;
    }

    foreach ($fields as $key => $field) {
        if (is_array($field)) {
            $field = filterLanguage($field, $lang);
        }
        if (strlen($key) - strpos($key, '|') === 3 && strpos($key, '|') > -1) {
            if ($lang == substr($key, -2)) {
                if (substr_compare($key, 'tab', -6, 3) !== 0) {
                    if (empty($field) && $lang != Config::mainLanguage()) {
                        $field = $fields[substr($key, 0, -2).Config::mainLanguage()];
                    }
                    $return[substr($key, 0, -3)] = $field;
                }
            }
        } else {
            $return[$key] = $field;
        }
    }
    return $return;
}

add_filter('acfcp/layoutData', 'acfcontentpressi18n\\filters\\filterLanguage', 10, 1);
