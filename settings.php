<?php
defined('ABSPATH') or die();
add_action('acf/init', 'add_option_page_acfcp_i18n');

function add_option_page_acfcp_i18n()
{
    acf_add_options_page(array(
            'page_title' => 'ACF CP I18n',
            'post_id' => 'acfcp_options_i18n'
        ));


    acf_add_local_field_group(array(
            'key' => 'group_59117f18b4e70',
            'title' => 'ACF ContentPress I18n Options',
            'fields' => array(
                array(
                    'key' => 'field_59117f2577d96',
                    'label' => 'Activate I18n',
                    'name' => 'activate_i18n',
                    'type' => 'true_false',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => '',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => 'field_59117f4e77d97',
                    'label' => 'Languages',
                    'name' => 'languages',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_59117f2577d96',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => '',
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'table',
                    'button_label' => 'Add Language',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_59117f6b77d98',
                            'label' => 'Language Short Code',
                            'name' => 'language_short_code',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => 2,
                        ),
                        array(
                            'key' => 'field_59117f8677d99',
                            'label' => 'Language Label',
                            'name' => 'language_label',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                        ),
                    ),
                ),
                array(
                    'key' => 'field_59117fb477d9a',
                    'label' => 'Main Language',
                    'name' => 'main_language',
                    'type' => 'text',
                    'instructions' => 'Shortcode',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_59117f2577d96',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '34',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => 2,
                ),
                array(
                    'key' => 'field_587654',
                    'label' => 'Restrict to Main Language',
                    'name' => 'use_main_language',
                    'type' => 'true_false',
                    'instructions' => 'Active this if you want your page to be served in the main language only, useful when launching with unfinished translations.',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_59117f2577d96',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '33',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => '',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => 'field_59137f2527394',
                    'label' => 'Main Language Fallback',
                    'name' => 'main_language_fallback',
                    'type' => 'true_false',
                    'instructions' => 'Active this to fall back to the field translation in the main language, if no value for the active language is found. A null-like value will trigger the fallback!',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_59117f2577d96',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '33',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => '',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => 'field_59333333',
                    'label' => 'Pass Through Routes',
                    'name' => 'pass',
                    'type' => 'repeater',
                    'instructions' => 'Specify routes that will not be parsed. See <a href="http://altorouter.com/usage/mapping-routes.html" target="_blank">Altorouter Docs</a> for syntax.',
                    'required' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => '',
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'table',
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_59117f2577d96',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'button_label' => 'Add Route',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_59117blb1312',
                            'label' => 'Method',
                            'name' => 'method',
                            'type' => 'select',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '25',
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array(
                                'GET' => 'GET',
                                'POST' => 'POST'
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => 122,
                        ),
                        array(
                            'key' => 'field_59117f6b1312',
                            'label' => 'Route',
                            'name' => 'route',
                            'type' => 'text',
                            'instructions' => 'include starting /',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '75',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => 122,
                        ),
                    ),
                )
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'acf-options-acf-cp-i18n',
                    ),
                ),
            ),
            'menu_order' => 9,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

    acf_add_local_field_group(array(
            'key' => 'group_59117f18b4e70_b',
            'title' => 'ACF ContentPress I18n Actions',
            'fields' => array(
                array(
                    'key' => 'field_regenerate_slugs',
                    'label' => 'Regenerate Slugs',
                    'name' => 'regenerate_slugs',
                    'type' => 'true_false',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => '',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'acf-options-acf-cp-i18n',
                    ),
                ),
            ),
            'menu_order' => 10,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
}
