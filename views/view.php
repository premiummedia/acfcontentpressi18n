<?php

namespace acfcontentpressi18n\views;

use acfcontentpressi18n\Config;
use acfcontentpressi18n\db\DBTranslations;
use acfcontentpressi18n\routing\Permalink;

abstract class View
{
    protected const CX_LANGUAGES = '_languages';
    protected const CX_ID = '_id';
    protected const CX_LANG = '_lang';
    protected const CX_TYPE = '_type';

    protected $context = [];

    protected $id = null;

    protected $type = '';

    protected $lang = null;

    public function __construct($id, $type, $lang){
        $this->id = $id;
        $this->type = $type;
        $this->lang = $lang;

        $this->setUpGlobals();
        $this->addI18nContext();
        $this->addIdContext();
        $this->addTypeContext();

        add_filter('acfcpfe/contentcontext', [$this, 'addContextToContents'], 1, 1);

        $this->render(
            $this->getTemplatePath(
                $this->getPossibleTemplatePaths() 
            )
        );
    }

    public function addContextToContents($context){
        return array_merge($context, ['view' => $this]);
    }

    public function __get($key){
        if(array_key_exists($key, $this->context)){
            return $this->context[$key];
        }
        return $this->$key;
    }

    abstract protected function getPossibleTemplatePaths();

    protected function getTemplatePath($possibleTemplatePaths){
       return get_query_template($this->type, $possibleTemplatePaths);
    }

    protected function setUpGlobals(){
        $GLOBALS['post'] = get_post($this->id);
    }

    protected function render($templatePath)
    {
        if (file_exists($templatePath)) {
            ob_start();
           
            // set up some wp globals
            global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;
            
            if (is_array($wp_query->query_vars)) {
                extract($wp_query->query_vars, EXTR_SKIP);
            }
            // wth wordpress?
            if (isset($s)) {
                $s = esc_attr($s);
            }
            $view = &$this;

            include($templatePath);
            
            echo ob_get_clean();
            return true;
        } else {
            exit('Template not found: '.$templatePath);
            return false;
        }
    }

    public function get_header($name = null){
        do_action('get_header', $name);

        $templates = [];
        $name = (string) $name;
        if( '' !== $name ){
            $templates[] = "header-{$name}.php";
        }
        $templates[] = "header.php";

        $template = locate_template( $templates );

        $this->render($template);
    }

    public function get_footer($name = null){
        do_action('get_footer', $name);
        
        $templates = [];
        $name = (string) $name;
        if( '' !== $name ){
            $templates[] = "footer-{$name}.php";
        }
        $templates[] = "footer.php";

        $template = locate_template( $templates );

        $this->render($template);

    }

    protected function addI18nContext()
    {
    
        $this->context[self::CX_LANGUAGES] = [];

        foreach (Config::languages() as $key => $label) {
            $url = Permalink::getPermalink($this->id, $this->type, $key);
            $this->context[self::CX_LANGUAGES][$key] = [
                'url' => $url,
                'label' => $label
            ];
        }

        $this->context[self::CX_LANG] = $this->lang; 

    }

    protected function addIdContext(){
        $this->context[self::CX_ID] = $this->id;
    }

    protected function addTypeContext(){
        $this->context[self::CX_TYPE] = $this->type;
    }


}
