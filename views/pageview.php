<?php

namespace acfcontentpressi18n\views;

use acfcontentpressi18n\views\View;

class PageView extends View
{
    protected function getPossibleTemplatePaths(){
        return [
            get_page_template_slug($this->id),
            'page.php'
        ];
    } 
}
