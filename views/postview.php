<?php

namespace acfcontentpressi18n\views;

use acfcontentpressi18n\views\View;

class PostView extends View
{
    protected function getPossibleTemplatePaths(){
        $cptSlug = get_post_field('post_name', $this->id);
        $cptSlugDecoded = urldecode($cptSlug);
    
        return [
            "single-{$this->type}-{$cptSlugDecoded}.php",
            "single-{$this->type}-{$cptSlug}.php",
            "single-{$this->type}.php",
            "single.php",
        ];
    }
}
