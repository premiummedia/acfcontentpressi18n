<?php

namespace acfcontentpressi18n\routing;

use acfcontentpressi18n\Config;
use acfcontentpressi18n\db\DBTranslations;

class Permalink{

    static function getPermalink($id, $type, $lang = null){

        if( !$lang ){
            $lang = Config::mainLanguage();
        }

        $base = get_bloginfo('url');

        $postLink = DBTranslations::i18nPath($id, $lang, $type);

        if( !$postLink ){
            return '';
        }

        return $base."/".$lang."/".$postLink;
    }

}
