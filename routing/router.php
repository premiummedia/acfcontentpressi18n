<?php

namespace acfcontentpressi18n\routing;

defined('ABSPATH') or die();

use AltoRouter;
use acfcontentpressi18n\Config;
use acfcontentpressi18n\db\DBTranslations;
use acfcontentpressi18n\routing\Controller;

class Router
{
    private $wpContinue = false;

    public function __construct()
    {
        add_filter('do_parse_request', array(&$this, 'parseRequest'), 1, 3);

        add_action('wp', array(&$this, 'stopWp'));
    }

    public function stopWp()
    {
        if( !$this->wpContinue ){
            if (headers_sent()) {
                die();
            } elseif (!is_admin()) {
                return $this->notFound();
            }
        }
    }

    public function parseRequest($parse, $wp, $eqv)
    {
        $router = new AltoRouter();

        $pass = Config::passRoutes();

        $langsToMatch = [];
        $langRestriction = Config::restrictTo();
        if( $langRestriction ){
            $langsToMatch[] = $langRestriction;
        }else{
            $langsToMatch = array_keys( Config::languages() );
        }
        $router->addMatchTypes(
            array(
                'l' => implode(
                    '|',
                    $langsToMatch
                )
            )
        );


        $router->map(
            'GET',
            '/',
            function () {
                $frontPageId = get_option('page_on_front');
                $acceptLanguage = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
                if( $override = Config::restrictTo() ){
                    $acceptLanguage = $override;
                }
                //wp_die($acceptLanguage);
                if (!in_array($acceptLanguage, array_keys(Config::languages()))) {
                    $acceptLanguage = Config::mainLanguage();
                }
                header("location: /".$acceptLanguage."/".DBTranslations::i18nPath($frontPageId, $acceptLanguage, 'page'));
                exit();
            }
        );

        $router->map(
            'GET',
            '/[l:language]',
            function ($language) {
                $frontPageId = get_option('page_on_front');
                header("location: /".$language."/".DBTranslations::i18nPath($frontPageId, $language, 'page'));
                exit();
            }
        );

        $router->map(
            'GET',
            '/[l:language]/[*:trailing]',
            function ($language, $trailing) {
                $path = explode('/', trim(stripslashes_deep($trailing), '/ '));
                $c = new Controller($path, $language);
                return $c->matchFound;
            }
        );

        $router->map(
            'GET',
            '/admin',
            function () {
                $this->redirectAdmin();
            }
        );

        foreach( Config::passRoutes() as $route ){
            $router->map(
                $route['method'],
                $route['route'],
                function(){
                    return 0;
                }
            );
        }

        $match = $router->match();

        $innerMatch = false;
        if ($match && is_callable($match['target'])) {
            $innerMatch = call_user_func_array($match['target'], $match['params']);
        }

        if ($innerMatch === false && !is_admin()) {
            $this->notFound();
        } else {
            if (!is_admin() && $innerMatch !== 0) {
                die();
            }
        }
        if( $innerMatch === 0 ){
            $this->wpContinue = true;
        }

        return $wp;
    }

    public function notFound()
    {
        global $wp_query;
        $wp_query->set_404();
        status_header(404);
        get_template_part(404);
        exit();
    }

    public function redirectAdmin()
    {
        wp_safe_redirect(admin_url());
        die();
    }
}
