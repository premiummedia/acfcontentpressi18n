<?php
namespace acfcontentpressi18n\routing;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;
use acfcontentpressi18n\db\DBTranslations;
use acfcontentpressi18n\views\PageView;
use acfcontentpressi18n\views\PostView;
use acfcontentpressi18n\routing\Permalink;

class Controller
{
    public $context = array();

    private $id;
    private $lang;
    private $type;

    public $matchFound = false;

    public function __construct($path, $lang)
    {
        $this->lang = $lang;

        $this->resolve($path, $lang);

        add_filter('acfcp/lang', array($this, 'lang'));
        
        add_filter('post_type_link', array($this, 'wireLinks'), 1, 2);
        add_filter('page_link', array($this, 'wireLinks'), 1, 2);
        add_filter('post_link', array($this, 'wireLinks'), 1, 2);

        $this->matchFound = $this->display();

    }

    public function wireLinks($url, $post){
        if( !is_object($post) ){
            $post = get_post($post);
        }
        return Permalink::getPermalink($post->ID, get_post_type($post), $this->lang);
    }

    public function lang(){
        return $this->lang;
    }

    public function display()
    {
        if( 'page' == $this->type && 
            (
                'publish' == get_post_status($this->id) ||
                is_user_logged_in()
            )
        ) {
            return new PageView($this->id, $this->type, $this->lang);
        } elseif ( 'page' != $this->type && 
            $this->id
        ) {
            return new PostView($this->id, $this->type, $this->lang);
        }
        return false;

    }

    public static function isCPT($slug, $lang)
    {
        return DBTranslations::i18nCptReverse($slug, $lang);
    }

    public static function isPage($slug, $parent, $lang)
    {
        return DBTranslations::i18nId($slug, $lang, 'page', $parent);
    }

    public static function getPathFragment($path, $parent = false, $lang)
    {
        return array(
            'path' => $path,
            'page' => self::isPage($path, $parent, $lang),
            'cpt' => self::isCPT($path, $lang)
        );
    }

    public function resolve($path, $lang)
    {
        $currentPathSlug = reset($path);

        $pageTrail = array();

        for ($i = 0; $i < sizeof($path); $i++) {
            $current = Controller::getPathFragment($path[$i], end($pageTrail), $lang);

            if ($current['page']) {
                $pageTrail[] = $current['page'];
            }

            $next = false;

            if (sizeof($path) > ($i + 1)) {
                $next = Controller::getPathFragment($path[$i + 1], $current['page'], $lang);
            }

            if ($current['page'] && !$next) {
                $this->id = $current['page'];
                $this->type = 'page';
                return;
            }

            if ($current['cpt'] && !$next) {

                // Display CPT Archive
                $this->type = $current['cpt'];
                return;
            }

            if ($current['cpt'] && !$next['page']) {
                if ($id = DBTranslations::i18nId($next['path'], $lang, $current['cpt'])) {

                    $this->id = $id;
                    $this->type = $current['cpt'];
                    return;
                }
            }
        }
        return false;
    }
}
