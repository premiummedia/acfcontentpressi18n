<?php

namespace acfcontentpressi18n\admin\filters;

use acfcontentpressi18n\Config;
use acfcontentpressi18n\routing\Permalink;

add_filter('post_type_link', 'acfcontentpressi18n\\admin\\filters\\getAdminPermalink', 1, 2);
add_filter('page_link', 'acfcontentpressi18n\\admin\\filters\\getAdminPermalink', 1, 2);
add_filter('post_link', 'acfcontentpressi18n\\admin\\filters\\getAdminPermalink', 1, 2);

function getAdminPermalink($url, $post){
    if( !is_object($post) ){
        $post = get_post($post);
    }
    $permalink = Permalink::getPermalink($post->ID, get_post_type($post), Config::mainLanguage());
    if( !empty($permalink) ){
        return $permalink;
    }
    return $url;
}
