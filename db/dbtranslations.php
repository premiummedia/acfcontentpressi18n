<?php
namespace acfcontentpressi18n\db;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

class DBTranslations
{
    public static function i18nPath($id, $language, $type)
    {
        $path = array();

        do {
            $s = DBTranslations::i18nSlug($id, $language, $type);
            if( $s ) array_push($path, $s);
        } while ($id = wp_get_post_parent_id($id));

        if ($type != 'page') {
            $cptSlug = DBTranslations::i18nCpt($type, $language);
            if( $cptSlug ) array_push($path, $cptSlug);
        }
        

        return implode('/', array_reverse($path));
    }

    public static function i18nPermalink($id, $language, $type)
    {
        $path = DBTranslations::i18nPath($id, $language, $type);
        return "//".$_SERVER['HTTP_HOST']."/".$language."/".$path;
    }

    public static function i18nSlug($id, $language = false, $type = false)
    {
        global $wpdb;

        $tableName = $wpdb->prefix.Config::$slugTranslationTable;

        if (!$language) {
            $language = current(array_keys(Config::languages()));
        }

        $query = 'SELECT slug FROM '.$tableName.' WHERE lang = "'.$language.'" AND post_id = "'.$id.'"';
        if ($type) {
            $query .= ' AND type = "'.$type.'"';
        }
        $slug = $wpdb->get_var($query);

        if ($slug) {
            return $slug;
        }

        return false;
    }

    public static function i18nId($slug, $language, $type = false, $parent = false)
    {
        global $wpdb;

        $tableName = $wpdb->prefix.Config::$slugTranslationTable;

        $query = 'SELECT post_id FROM '.$tableName.' WHERE lang = "'.$language.'" AND slug = "'.$slug.'"';
        if ($type) {
            $query .= ' AND type = "'.$type.'"';
        }
        if ($parent) {
            $query .= ' AND parent = '.$parent;
        }
        $id = $wpdb->get_var($query);


        if ($id) {
            return $id;
        }

        return false;
    }

    public static function i18nTermIdTax($slug, $language)
    {
        global $wpdb;

        $tableName = $wpdb->prefix.Config::$slugTranslationTable;

        $query = 'SELECT id, type FROM '.$tableName.' WHERE lang = "'.$language.'" AND slug = "'.$term.'" AND NOT type = "page"';

        $idAndTax = $wpdb->get_row($query, ARRAY_A);

        if ($idAndTax) {
            return $idAndTax;
        }

        return false;
    }

    public static function i18nCpt($slug, $language)
    {
        global $wpdb;

        $tableName = $wpdb->prefix.Config::$cptSlugTranslationTable;

        $query = 'SELECT slug FROM '.$tableName.' WHERE lang = "'.$language.'" AND cpt = "'.$slug.'"';

        $cpt = $wpdb->get_var($query);

        if ($cpt) {
            return $cpt;
        }

        return false;
    }

    public static function i18nCptReverse($slug, $language)
    {
        global $wpdb;

        $tableName = $wpdb->prefix.Config::$cptSlugTranslationTable;

        $query = 'SELECT cpt FROM '.$tableName.' WHERE lang = "'.$language.'" AND slug = "'.$slug.'"';

        $cpt = $wpdb->get_var($query);

        if ($cpt) {
            return $cpt;
        }

        return false;
    }



    public static function i18nTaxonomy($slug, $langauge)
    {
        global $wpdb;

        $tableName = $wpdb->prefix.Config::$cptSlugTranslationTable;

        $query = 'SELECT taxonomy FROM '.$tableName.' WHERE lang = "'.$language.'" AND slug = "'.$slug.'"';

        $taxonomy = $wpdb->get_var($query);

        if ($taxonomy) {
            return $taxonomy;
        }
        return false;
    }
}
