<?php
namespace acfcontentpressi18n\db;

defined('ABSPATH') or die();

use acfcontentpressi18n\Config;

global $wpdb;

$charsetCollate = $wpdb->get_charset_collate();

setupDBPostSlugs($charsetCollate);
setupDBPostTypeSlugs($charsetCollate);
setupDBTaxonomySlugs($charsetCollate);

function setupDBPostSlugs($charsetCollate)
{
    global $wpdb;

    $tableName = $wpdb->prefix.Config::$slugTranslationTable;
    $tableExists = $wpdb->get_var("SHOW TABLES LIKE '".$tableName."'");

    if (!$tableExists) {
        $sql = "CREATE TABLE $tableName (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            post_id mediumint(9) NOT NULL,
            type varchar(64) DEFAULT '' NOT NULL,
            lang varchar(8) DEFAULT '' NOT NULL,
            slug varchar(125) DEFAULT '' NOT NULL,
            parent mediumint(9) DEFAULT NULL,
            UNIQUE KEY id (id)
        ) $charsetCollate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

function setupDBPostTypeSlugs($charsetCollate)
{
    global $wpdb;

    $tableName = $wpdb->prefix.Config::$cptSlugTranslationTable;
    $tableExists = $wpdb->get_var("SHOW TABLES LIKE '".$tableName."'");

    if (!$tableExists) {
        $sql = "CREATE TABLE $tableName (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            cpt varchar(64) DEFAULT '' NOT NULL,
            lang varchar(8) DEFAULT '' NOT NULL,
            slug varchar(125) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
        ) $charsetCollate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

function setupDBTaxonomySlugs($charsetCollate)
{
    global $wpdb;

    $tableName = $wpdb->prefix.Config::$taxonomySlugTranslationTable;
    $tableExists = $wpdb->get_var("SHOW TABLES LIKE '".$tableName."'");

    if (!$tableExists) {
        $sql = "CREATE TABLE $tableName (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            taxonomy varchar(64) DEFAULT '' NOT NULL,
            lang varchar(8) DEFAULT '' NOT NULL,
            slug varchar(125) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
        ) $charsetCollate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
