<?php
namespace acfcontentpressi18n\navigation;

defined('ABSPATH') or die();

use acfcontentpressi18n\navigation\MenuItem;
use acfcontentpressi18n\navigation\MenuItemCollection;

class Navigation
{
    private $lang = '';
    private $id = null;

    private $menu = null;

    private $menuItems;

    private $defaultDisplaySettings = array(
        'wrapperEl' => array(
            'el' => 'li',
            'activeClass' => 'active',
            'inactiveClass' => 'inactive',
            'hasChildrenClass' => 'parent',
            'classes' => array(),
            'attributes' => array()
        ),
        'linkEl' => array(
            'el' => 'a',
            'activeClass' => '',
            'inactiveClass' => '',
            'classes' => array(),
            'attributes' => array()
        ),
        'listEl' => array(
            array(
                'el' => 'ul',
                'classes' => array('toplevel'),
                'attributes' => array()
            ),
            array(
                'el' => 'ul',
                'classes' => array('sublevel'),
                'attributes' => array()
            ),
            array(
                'el' => 'ul',
                'classes' => array('subsublevel'),
                'attributes' => array()
            ),
            array(
                'el' => 'ul',
                'classes' => array('subsubsublevel'),
                'attributes' => array()
            ),
            array(
                'el' => 'ul',
                'classes' => array('subsubsubsublevel'),
                'attributes' => array()
            )
        )
    );

    private $wrapperElSettings = array();
    private $linkElSettings = array();
    private $listElSettings = array();

    public function __construct($id, $lang, $menu, $wrapperElSettings = array(), $linkElSettings = array(), $listElSettings = array())
    {
        $this->lang = $lang;
        $this->id = $id;

        $this->wrapperElSettings = array_replace_recursive(
            $this->defaultDisplaySettings['wrapperEl'],
        $wrapperElSettings
        );
        $this->linkElSettings = array_replace_recursive($this->defaultDisplaySettings['linkEl'], $linkElSettings);
        $this->listElSettings = array_replace_recursive($this->defaultDisplaySettings['listEl'], $listElSettings);


        $menuTerm = get_terms(
            array(
                'taxonomy' => 'nav_menu',
                'slug' => $menu
            )
        );

        if (count($menuTerm) == 0) {
            throw new \Exception("Menu '".$menu."' not found.");
        }


        $this->menu = reset($menuTerm)->term_id;

        $this->menuItems = new MenuItemCollection(
            wp_get_nav_menu_items($this->menu),
            $this->id,
            $this->lang
        );
    }

    public function navId($pageId)
    {
        return $this->menuItems->getNavIdForPageId($pageId);
    }

    public function nav($root = 0, $depth = null, $currentDepth = 0)
    {
        $nav = array();

        if ($depth != null && $depth <= $currentDepth) {
            if (empty($this->menuItems->getIterator($root))) {
                return false;
            } else {
                return true;
            }
        }

        foreach ($this->menuItems->getIterator($root) as $menuItem) {
            array_push(
                $nav,
                array(
                    'item' => $menuItem,
                    'children' => $this->nav($menuItem->id, $depth, $currentDepth + 1)
                )
            );
        }

        return $nav;
    }

    private function elAttributesToString($atts)
    {
        $out = '';
        foreach ($atts as $key => $value) {
            $out .= $key.'="'.$value.'" ';
        }
        return $out;
    }

    private function elClassesToString($classes)
    {
        return " ".implode(" ", $classes)." ";
    }

    public function display($root = 0, $depth = null)
    {
        $this->displayLevel($this->nav($root, $depth), array_slice($this->listElSettings, $this->menuItems->getDepth($root)));
    }

    public function displayLevel($items, $subLevelSettings)
    {
        if (!$items || !is_array($items)) {
            return;
        }
        $currentLevelSettings = array_shift($subLevelSettings);
        echo '<'.$currentLevelSettings['el'].' class="'.$this->elClassesToString($currentLevelSettings['classes']).'" '.$this->elAttributesToString($currentLevelSettings['attributes']).'>';

        foreach ($items as $item) {
            $this->displayItem($item, $subLevelSettings);
        }

        echo '</'.$currentLevelSettings['el'].'>';
    }

    public function displayItem($item, $subLevelSettings = array(), $wrapperElSettings = array(), $linkElSettings = array())
    {
        $children = $item['children'];
        $item = $item['item'];

        $wrapperElSettings = $this->wrapperElSettings + $wrapperElSettings;
        $linkElSettings = $this->linkElSettings + $linkElSettings;

        $classes = $wrapperElSettings['classes'];

        $classes += (is_array($children) && sizeof($children) > 0) ? array($wrapperElSettings['hasChildrenClass']) : array();

        $classes += ($item->objectId == $this->id) ? array($wrapperElSettings['activeClass']) : array();

        echo '<'.$wrapperElSettings['el'].' class="'.$this->elClassesToString($classes).'" '.$this->elAttributesToString($wrapperElSettings['attributes']).'>';

        echo '<'.$linkElSettings['el'].' class="'.$this->elClassesToString($linkElSettings['classes']).'" '.$this->elAttributesToString($linkElSettings['attributes']).' href="'.$item->permalink.'" target="'.$item->target.'">';
        echo $item->label;
        echo '</'.$linkElSettings['el'].'>';
        if (!!$children) {
            $this->displayLevel($children, $subLevelSettings);
        }

        echo '</'.$wrapperElSettings['el'].'>';
    }
}
