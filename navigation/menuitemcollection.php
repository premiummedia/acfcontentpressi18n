<?php
namespace acfcontentpressi18n\navigation;

defined('ABSPATH') or die();

use acfcontentpressi18n\navigation\MenuItem;

class MenuItemCollection
{
    private $menuItems = array();

    public function __construct($wpMenuItems, $id, $lang)
    {
        $this->menuItems = array_map(
            function ($wpItem) use ($id, $lang) {
                return new MenuItem($wpItem, $id, $lang);
            },
            $wpMenuItems
        );

        $this->markCurrentPath();
    }

    public function getIterator($parent)
    {
        $data = array_filter(
            $this->menuItems,
            function ($item) use ($parent) {
                return $item->parent == $parent;
            }
        );
        if (!empty($data)) {
            return new \ArrayIterator($data);
        }
        return array();
    }

    public function getNavIdForPageId($pageId)
    {
        $data = array_filter(
            $this->menuItems,
            function ($item) use ($pageId) {
                return $item->objectId == $pageId;
            }
        );

        if (is_array($data) && count($data) == 1) {
            return current($data)->id;
        }

        return false;
    }

    public function findCurrent()
    {
        $data = array_filter(
            $this->menuItems,
            function ($item) {
                return $item->current == true;
            }
        );

        if (is_array($data) && count($data) == 1) {
            return reset($data);
        }

        return false;
    }

    public function find($id)
    {
        $data = array_filter(
            $this->menuItems,
            function ($item) use ($id) {
                return $item->id == $id;
            }
        );

        if (is_array($data) && count($data) == 1) {
            return reset($data);
        }

        return false;
    }

    public function markCurrentPath($current = false)
    {
        if (!$current) {
            $current = $this->findCurrent();
        }

        if (isset($current->parent)) {
            $parent = $this->find($current->parent);

            if ($parent) {
                $parent->currentParent = true;
                $this->markCurrentPath($parent);
            }
        }
    }

    public function getDepth($id)
    {
        if (!$id) {
            return 0;
        }

        $depth = 0;
        while ($data = $this->find($id)) {
            $depth++;
            $id = $data->parent;
        }
        return $depth-1;
    }
}
