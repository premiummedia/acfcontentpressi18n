<?php
namespace acfcontentpressi18n\navigation;

defined('ABSPATH') or die();

use acfcontentpressi18n\db\DBTranslations;

class MenuItem
{
    public $id = null;
    public $objectId = null;
    public $label = '';
    public $permalink = '';
    public $current = null;
    public $currentParent = null;
    public $currentAncestor = null;
    public $parent = null;
    public $target = "";

    public function __construct($menuItem, $currentId, $lang)
    {
        $this->id = $menuItem->ID;
        $this->objectId = $menuItem->object_id;
        $langLabel = $this->getLabel($lang);
        $this->label = ($langLabel) ? $langLabel : $menuItem->title;
        $this->permalink = $this->getLink($menuItem->object_id, $lang);
        if ($menuItem->type == "custom" && $menuItem->url) {
            $this->permalink = $menuItem->url;
        }

        if ($currentId == $menuItem->object_id) {
            $this->current = true;
        } else {
            $this->current = false;
        }
        $this->parent = $menuItem->menu_item_parent;
        $this->target = $menuItem->target;
    }

    public function getLabel($lang)
    {
        return get_post_meta($this->id, '_menu_item_label_'.$lang, true);
    }

    public function getLink($objectId, $lang)
    {
        $type = get_post_type($objectId);
        return DBTranslations::i18nPermalink($objectId, $lang, $type);
    }
}
