<?php
namespace acfcontentpressi18n\navigation;

defined('ABSPATH') or die();

use acfcontentpressi18n\navigation\MenuItem;

class Breadcrumbs
{
    private $lang = '';
    private $id = null;
    private $start = null;

    public function __construct($id, $lang)
    {
        $this->lang = $lang;
        $menuItemQueryArgs = array(
            'post_type' => 'nav_menu_item',
            'post_status' => 'publish',
            'output' => ARRAY_A,
            'output_key' => 'menu_order',
            'nopaging' => true,
            'meta_key' => '_menu_item_object_id',
            'meta_value' => $id
        );

        $item = $this->setupMenuItem(current(get_posts($menuItemQueryArgs)));
        $this->start = $item;
    }

    public function getCrumbs($excludeCurrent = false)
    {
        $crumbs = array();
        if (!$excludeCurrent) {
            array_push(
                $crumbs,
                $this->start
            );
        }
        if ($item = $this->getMenuItem($this->start->parent)) {
            do {
                array_push(
                    $crumbs,
                    $item
                );
                $item = $this->getMenuItem($item->parent);
            } while ($item);
        }
        return array_reverse($crumbs);
    }

    public function getMenuItem($menuItemId)
    {
        if (!$menuItemId || $menuItemId === "0") {
            return false;
        }

        $queryArgs = array(
            'post_type' => 'nav_menu_item',
            'post_status' => 'publish',
            'output' =>  ARRAY_A,
            'output_key' => 'menu_order',
            'nopaging' => true,
            'p' => $menuItemId
        );
        $posts = get_posts($queryArgs);


        if (sizeof($posts) == 0) {
            return false;
        }

        return $this->setupMenuItem(current($posts));
    }

    public function setupMenuItem($item)
    {
        if (!$item) {
            return false;
        }
        $item = wp_setup_nav_menu_item($item);
        return new MenuItem($item, null, $this->lang);
    }
}
