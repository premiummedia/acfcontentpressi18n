<?php
defined('ABSPATH') or die();

spl_autoload_register(function ($klass) {
    if (strpos($klass, 'acfcontentpressi18n') === 0) {
        $klass = str_replace('acfcontentpressi18n', '', $klass);
        $klass = dirname(__FILE__) . '/' .str_replace('\\', '/', strtolower($klass)) . '.php';

        if (file_exists($klass)) {
            require_once $klass;
        }
    }
});
